from setuptools import setup, find_namespace_packages
from pathlib import Path

path_to_core = Path(__file__).parent.parent / 'core'
setup(
    name='sensitivity',
    version='1.0.0',
    packages=find_namespace_packages(include=['datafusiontools.*']),
    install_requires=["SALib",
                      'core@git+https://bitbucket.org/DeltaresGEO/datafusiontools.git@namespace-packages#subdirectory=DataFusionTools/core']

)

# Keep method find_namespace_packages to be able to do import datafusiontools.sensitivity.sensitivity
