from pathlib import Path

from setuptools import setup, find_namespace_packages

path_to_core = Path(__file__).parent.parent / 'core'

setup(
    name='interpolation',
    version='1.0.0',
    packages=find_namespace_packages(include=['datafusiontools.*']),
    install_requires=['shapely>=1.8', 'scipy>=1.9', 'numpy>=1.22', 'scikit-learn>=1.0', 'pykrige>=1.7', 'spatial_utils',
                      f"core @ file://localhost/{path_to_core}#egg=core"]

)
