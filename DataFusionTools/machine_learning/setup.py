from pathlib import Path

from setuptools import setup, find_namespace_packages
path_to_core = Path(__file__).parent.parent / 'core'

setup(
    name='machine_learning',
    version='1.0.0',
    packages=find_namespace_packages(include=['datafusiontools.*']),
    install_requires=['scipy>=1.9', 'numpy>=1.22', 'scikit-learn>=1.0', 'tensorflow>=2.6',
                      'tensorflow-probability>=0.17', 'shap>=0.39', 'sompy@git+https://github.com/sevamoo/SOMPY.git',
                      'scikit-image>=0.19', 'core@git+https://bitbucket.org/DeltaresGEO/datafusiontools.git@namespace-packages#subdirectory=DataFusionTools/core']

)
