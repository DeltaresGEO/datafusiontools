from setuptools import setup, find_namespace_packages

setup(
    name='d_series_parser',
    version='1.0.0',
    packages=find_namespace_packages(include=['datafusiontools.*']),
    install_requires=['d-geolib>=0.1.7', 'shapely>=1.8', 'scipy>=1.9', 'numpy>=1.22', 'topojson>=1.4', 'scikit-learn>=1.0']

)
