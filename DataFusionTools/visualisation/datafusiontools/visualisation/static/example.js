const backgroundLayers = {};
const userLayers = {};
const viewer = createViewer();
// viewer.extend(Cesium.viewerCesium3DTilesInspectorMixin);
// const inspectorViewModel = viewer.cesium3DTilesInspector.viewModel;

addBackgroundLayer(
    "Aerial HR",
    "https://service.pdok.nl/hwh/luchtfotorgb/wmts/v1_0?request=GetCapabilities&service=wmts",
    "2021_orthoHR",
    "image/png",
    true
);

addBackgroundLayer(
    "BGT",
    "https://service.pdok.nl/brt/achtergrondkaart/wmts/v2_0",
    "grijs",
    "image/png"
);

if (viewModel?.tilesets) {
    for (let i = 0; i < viewModel.tilesets.length; i++) {
        addUserLayer(viewModel.tilesets[i], i == 0);
    }
}

listenTransparancyChange();

//flyTo example, already zooming automatically to first tileset
//flyTo(6.599192, 53.298561, 412.5886013073694, 329, -29.189434822434666, 0);

function createViewer() {
    var viewer = new Cesium.Viewer("cesiumContainer", {
        animation: false,
        timeline: false,
        creditDisplay: false,
        shadows: false,
        geocoder: false,
        homeButton: false,
        sceneModePicker: false,
        navigationHelpButton: false,
        requestRenderMode: true,
        baseLayerPicker: false,
        sceneModePicker: false,
        fullscreenButton: false,
        imageryProvider: false,
        infoBox: true,
    });

    viewer.imageryLayers.removeAll();
    viewer.scene.globe.depthTestAgainstTerrain = true;
    viewer.scene.screenSpaceCameraController.enableCollisionDetection = false;
  
    return viewer;
}

function addBackgroundLayer(name, url, layer, format, show = false) {
    const provider = new Cesium.WebMapTileServiceImageryProvider({
        url: url,
        layer: layer,
        style: "default",
        format: format,
        tileMatrixSetID: "EPSG:3857",
        tileMatrixLabels: [
            "EPSG:3857:0",
            "EPSG:3857:1",
            "EPSG:3857:2",
            "EPSG:3857:3",
            "EPSG:3857:4",
            "EPSG:3857:5",
            "EPSG:3857:6",
            "EPSG:3857:7",
            "EPSG:3857:8",
            "EPSG:3857:9",
            "EPSG:3857:10",
            "EPSG:3857:11",
            "EPSG:3857:12",
            "EPSG:3857:13",
            "EPSG:3857:14",
            "EPSG:3857:15",
            "EPSG:3857:16",
            "EPSG:3857:17",
            "EPSG:3857:18",
            "EPSG:3857:19"
        ],
        tilingScheme: new Cesium.WebMercatorTilingScheme({
            ellipsoid: Cesium.Ellipsoid.WGS84,
        }),
        maximumLevel: 19,
        tileWidth: 256,
        tileHeigth: 256
    });

    backgroundLayers[name] = new Cesium.ImageryLayer(provider);
    backgroundLayers[name].show = show;
    viewer.imageryLayers.add(backgroundLayers[name], 0);
    addBackgroundOption(name, show);
}

function addBackgroundOption(name, show) {
    const bgLayerselement = document.querySelector("#background-layers");
    var bgLayer = document.createElement("div");
    bgLayer.classList.add("background-layer");
    var radiobox = document.createElement("input");
    radiobox.type = "radio";
    radiobox.id = name;
    radiobox.name = "bg";
    radiobox.checked = show;
    radiobox.addEventListener("change", () => {
        switchBackgroundLayer(name);
    });

    var label = document.createElement("label");
    label.htmlFor = name;
    label.appendChild(document.createTextNode(name));

    bgLayer.appendChild(radiobox);
    bgLayer.appendChild(label);
    bgLayerselement.appendChild(bgLayer);
}

function switchBackgroundLayer(name) {
    const keys = Object.keys(backgroundLayers);
    for (let i = 0; i < keys.length; i++) {
        backgroundLayers[keys[i]].show = false;
    }

    backgroundLayers[name].show = true;
    refreshMap();
}

async function addUserLayer(entry, show) {
    const colorbarUrl = entry.colorbar;
    let colorBar;
    await fetch(colorbarUrl)
        .then((response) => response.json())
        .then((data) => (colorBar = data));
    const tileset = new Cesium.Cesium3DTileset({
        url: entry.resource,
    });

    const conditionsColors = createTileStyle(colorBar);

    tileset.maximumScreenSpaceError = 16.0;
    tileset.pointCloudShading.maximumAttenuation = undefined; // Will be based on maximumScreenSpaceError instead
    tileset.pointCloudShading.baseResolution = undefined;
    tileset.pointCloudShading.geometricErrorScale = 1.0;
    tileset.pointCloudShading.attenuation = true;
    tileset.pointCloudShading.eyeDomeLighting = true;
    console.log(createTileStyle(colorBar));
    userLayers[entry.name] = tileset;
    tileset.style = new Cesium.Cesium3DTileStyle({
            color:{ 
                    conditions : conditionsColors
                }
    });
    tileset.show = show;
    viewer.scene.primitives.add(tileset);

    if (show) {
        tileset.readyPromise
            .then(function () {
                viewer.zoomTo(tileset);
            })
            .catch(function (error) {
                throw error;
            });
    }

    addUserLayerOption(entry.name, show, colorBar);
}

async function addUserLayerOption(name, show, colorBar) {
    const colorStops = colorBar.stops;
    const userLayerselement = document.querySelector("#user-layers");
    const layerWrapper = document.createElement("div");
    layerWrapper.classList.add("user-layer");

    const checkboxDiv = document.createElement("div");
    checkboxDiv.classList.add("cb");

    const cb = document.createElement("input");
    cb.type = "checkbox";
    cb.id = name;
    cb.name = "user";
    cb.checked = show;
    cb.addEventListener("change", (e) => {
        changeVisibilityUserLayer(name, e);
    });

    const label = document.createElement("label");
    label.htmlFor = name;
    label.appendChild(document.createTextNode(name));

    checkboxDiv.appendChild(cb);
    checkboxDiv.appendChild(label);
    layerWrapper.appendChild(checkboxDiv);

    const legend = await createLegend(colorBar);
    layerWrapper.appendChild(legend);

    // Add values below colorbar
    const legendValues = document.createElement("div");
    legendValues.classList.add("legend-values");

    const startValue = document.createElement("p");
    startValue.appendChild(document.createTextNode(Math.round(colorStops[0].value*100)/100));
    legendValues.appendChild(startValue);

    const endValue = document.createElement("p");
    endValue.appendChild(
        document.createTextNode(Math.round(colorStops[colorStops.length - 1].value*100)/100)
    );
    legendValues.appendChild(endValue);

    layerWrapper.appendChild(legendValues);
    userLayerselement.appendChild(layerWrapper);
}

function changeVisibilityUserLayer(name, event) {
    const layer = userLayers[name];
    layer.show = event.target.checked;
    refreshMap();
}

async function createLegend(colorBar) {
    const colorStops = colorBar.stops;
    const c = document.createElement("canvas");
    c.classList.add("legend-canvas");
    var ctx = c.getContext("2d");
    var grd = ctx.createLinearGradient(0, 0, c.width, 0);

    const interval = 1 / (colorStops.length - 1);
    grd.addColorStop(0, colorStops[0].color);

    for (let i = 1; i < colorStops.length - 1; i++) {
        grd.addColorStop(i * interval, colorStops[i].color);
    }

    grd.addColorStop(1, colorStops[colorStops.length - 1].color);
    ctx.fillStyle = grd;
    ctx.fillRect(0, 0, c.width, c.height);

    return c;
}

function colorStopsToConditions(colorBar) {
    const conditions = [];
    for (let i = 0; i < colorStops.length; i++) {
        const element = colorStops[i];
        conditions.push([condition,color])
        
    }
}

function createTileStyle(colorBar) {
    const conditions = [];
    const property = colorBar.label;
    const stops = colorBar.stops;
    
    for(let i = 0; i < stops.length; i++) {
        let conditionCheck = `\${${property}} ${i === stops.length -1 ? ">=" : ">="} ${stops[i].value}`
        if(i !== stops.length -1) {
            conditionCheck = `${conditionCheck} && \${${property}} < ${stops[i + 1].value}`
        }

        let condition = [
            conditionCheck,
            `color("${stops[i].color}")`
        ];

        conditions.push(condition);
    }

    return conditions;
}

function listenTransparancyChange() {
    const transparancySlider = document.querySelector("#transparancy-slider");
    transparancySlider.addEventListener("input", (e) => {
        setGlobeOpacity(e.target.value);
    });
}

function refreshMap() {
    if (viewer.clock.shouldAnimate === false) {
        viewer.clock.shouldAnimate = true;
        viewer.clock.tick();
        viewer.clock.shouldAnimate = false;
    }
}

function flyTo(x, y, z, heading, pitch, duration) {
    viewer.camera.flyTo({
        destination: Cesium.Cartesian3.fromDegrees(x, y, z),
        orientation: {
            heading: Cesium.Math.toRadians(heading),
            pitch: Cesium.Math.toRadians(pitch),
            roll: 0.0,
        },
        duration: duration,
    });
}

function setGlobeOpacity(value) {
    const alpha = value / 100;

    if (alpha >= 1.0) {
        viewer.scene.globe.translucency.enabled = false;
    } else {
        viewer.scene.globe.translucency.enabled = true;
    }

    viewer.scene.globe.translucency.frontFaceAlpha = alpha;
    refreshMap();
}
