import numpy as np
from .serve import CesiumServer
from matplotlib.colors import Colormap, rgb2hex
import matplotlib.cm as cm
from typing import List, Union
import os
from .voxeltiles import VoxelTiles

DIRNAME = os.path.dirname(__file__)




class CesiumViewer:
    """
    Cesium online viewer for datafusion results
    """

    def __init__(self, result_folder: str, port: int = 8787):
        self.result_folder = os.path.abspath(result_folder)

        tiles_directory = os.path.realpath(
            os.path.join(self.result_folder, 'tiles', str(port)))
        self.cesium_server = CesiumServer(
            tiles_directory=tiles_directory, port=port)

        self.voxel_tiles = None

        os.makedirs(self.cesium_server.tiles_directory, exist_ok=True)

        self.cmap = None
        self.result_names = []

    @property
    def voxel_tiles(self):
        return self._voxel_tiles

    @voxel_tiles.setter
    def voxel_tiles(self, value):
        if not isinstance(value, (VoxelTiles, type(None))):
            raise TypeError('voxel_tiles must be a VoxelTiles')
        self._voxel_tiles = value


    def generate_voxel_tiles(self, xyz: np.ndarray, dxdydz: np.ndarray, values: dict, result: int, name: str, cmap: Union[str, Colormap] = None):
        """
        Create VoxelTiles object and attach it to the Cesium viewer.

        Parameters
        ----------
        xyz: array-like, shape (n, 3)
            Voxel points positions.
        dxdydz: array-like, shape (n, 3)
            Voxels size
        values: dict
            dictionary holding voxel data / metadata
        result: int
            key of the column of the values you want to visualize 
        cmap : str or `~matplotlib.colors.Colormap`, default: 'gist_rainbow'
            A `.Colormap` instance or registered colormap name. *cmap* is only
            used if *c* is an array of floats.
        """
        self.voxel_tiles = VoxelTiles(xyz, dxdydz, values, result, cmap)
        self.voxel_tiles.voxel_models
        self.voxel_tiles.tiles_directory = os.path.realpath(
            os.path.join(self.cesium_server.tiles_directory, name))
        self.result_names.append(name)
