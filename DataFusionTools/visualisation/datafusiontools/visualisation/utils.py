import numpy as np
from matplotlib.colors import Colormap, rgb2hex
import matplotlib.cm as cm
import pandas as pd

from numbers import Number
from typing import List, Union

# new method
# def _make_colors_dict(cmap: List[Union[str,Colormap]],values):
#     """
#     Create color dictionary {value:, color:} from matplotlib cmap and min max value.

#     Parameters
#     ----------
#     values : dictionary of values with 'label_names' & 'label_values'
#     cmap : list of str or `~matplotlib.colors.Colormap`, default: 'gist_rainbow'
#         A `.Colormap` instance or registered colormap name. *cmap* is only
#         used if *c* is an array of floats.

#     return [{'label':, stops:[{value:, color:}]}]: List[dict]
#     """
#     if len(cmap) < len(values['label_names']):
#         cmap += ['gist_rainbow'] * (len(values['label_names']) - len(cmap))
#     else:
#         cmap = cmap[:len(values['label_names'])]

#     cmap_tmp = [cm.get_cmap(cmp, 10) for cmp in cmap]
#     colors = np.array([np.apply_along_axis(rgb2hex,1,cmp(np.arange(cmp.N))) for cmp in cmap_tmp])
#     #values_quantiles = np.quantile(values['label_values'],np.arange(11)/10,axis=1)[1:].T
#     values_quantiles = np.apply_along_axis(lambda x: np.linspace(min(x), max(x), 11),1,values['label_values'])[:,1:]

#     labels = values['label_names']
#     s = pd.DataFrame([(l,c,v) for l,c, v in zip(labels,colors,values_quantiles)],columns=['label','colors','values']).explode(['colors','values']).groupby('label')[['colors','values']].apply(lambda x: x.to_dict(orient='records'))
#     df = pd.DataFrame({'label':s.index, 'stops':s.values})

#     return df.to_dict(orient='records')

# old method


def _make_colors_dict(cmap: Union[str, Colormap], label: str, vmin: Number, vmax: Number):
    """

    Create color dictionary {label:, value:, color:} from matplotlib cmap and min max value.


    Parameters

    ----------
    name : str, default: 
    vmin, vmax : float, default: None

        *vmin* and *vmax* are used in conjunction with the default norm to

        map the color array *c* to the colormap *cmap*. If None, the

        respective min and max of the color array is used.

    cmap : str or `~matplotlib.colors.Colormap`, default: 'gist_rainbow'

        A `.Colormap` instance or registered colormap name. *cmap* is only

        used if *c* is an array of floats.


    return [{value:, color:}]: List[dict]

    """

    cmap_tmp = cm.get_cmap(cmap, 10)

    colors_dict = []

    for i, val in enumerate(np.linspace(vmin, vmax, cmap_tmp.N)):

        rgba = cmap_tmp(i)

        colors_dict.append(
            {'value': val, 'color': rgb2hex(rgba)})

    return colors_dict
