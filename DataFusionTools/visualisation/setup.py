from setuptools import setup, find_namespace_packages

setup(
    name='visualisation',
    version='1.0.0',
    packages=find_namespace_packages(include=['datafusiontools.*']),
    install_requires=['numpy', 'flask>=2.1', 'typeguard>=2.13', 'open3d>=0.15', 'pyproj>=3.3', 'py3dtiles@git+https://gitlab.com/Oslandia/py3dtiles']
)
