from setuptools import setup, find_namespace_packages

setup(
    name='spatial_utils',
    version='1.0.0',
    packages=find_namespace_packages(include=['datafusiontools.*']),
    install_requires=['numpy', 'requests', 'rasterio', 'shapely']

)
