import pytest
import numpy as np
import pickle
import requests

from datafusiontools.visualisation.cesium import CesiumViewer
from tests.utils import TestUtils

class TestVisualisation:
    @pytest.mark.skip(reason="The initialisation of teh server is not sucessfull in the bitbucket pipeline")
    def test_visualisation(self):

        # http://www.open3d.org/docs/release/getting_started.html
        #!pip install open3d
        input_files = str(
            TestUtils.get_test_files_from_local_test_dir("", "test_visualisation.pickle")[0]
        )
        with open(input_files,'rb') as f:
            [idx, x, y, z, dx, dy, dz, values] = pickle.load(f)
        xyz = np.vstack((x, y, z)).transpose()
        dxdydz = np.vstack((dx, dy, dz)).transpose()
        # ## Initiate CesiumViewer
        viewer = CesiumViewer(result_folder='./data/', port=8080)
        # ## Generate voxel 3D data from result
        # ### Object in-memory (attached to CesiumViewer)
        viewer.generate_voxel_tiles(xyz,dxdydz,values,0,'test_voxel_0','viridis')
        # ### Write b3dm tile on disk
        viewer.voxel_tiles.write_tiles()
        assert viewer.voxel_tiles.tiles_directory is not None
        ## Start Cesium server
        viewer.cesium_server.start_server()
        assert requests.get(viewer.cesium_server.url).status_code == 200
        ## Stop Cesium server
        viewer.cesium_server.stop_server()





