import pytest
import numpy as np
import typing

from datafusiontools.machine_learning.mpl import MPL


class TestValidation:
    @pytest.mark.unittest
    def test_failing_validation(self):
        with pytest.raises(ValueError) as excinfo:
            MPL(classification=True, optimizer="wrong type", kl="wrong")
        assert (
            str(excinfo.value).find(
                "<class 'str'>' instead of 'typing.Union[typing.List, NoneType, numpy.ndarray]"
            )
            != -1
        )
